from urllib.request import urlopen
import json
import sys
from setting.path_url import Path_url
from  modbus_ph import Modbus_PH
sys.path.append('/home/pi/hottub_cocoon_apf/orp/')
from modbus_orp import Modbus_ORP
sys.path.append('/home/pi/hottub_cocoon_apf/apf/')
from modbus_apf import Modbus_APF
sys.path.append('/home/pi/hottub_cocoon_apf/inj/')
from mod_inj import Mod_inj

path_url = Path_url()
url_substance = path_url.url_substance
url_ph = path_url.url_ph
url_orp = path_url.url_orp
url_apf = path_url.url_apf
url_inj = path_url.url_inj

modbus_ph = Modbus_PH()
modbus_orp = Modbus_ORP()
modbus_apf = Modbus_APF()
mod_inj = Mod_inj()

class Main_PH():
    current_time = ''
    counter_ph = 0 
    counter_orp = 0
    counter_apf = 0
    read_ph_address = 0
    read_orp_address = 0
    set_relay = ''
    plc_all_out = ''
    def __init__(self, current_time, set_ph, set_orp, set_relay, plc_all_out):
        
        self.current_time = current_time
        self.read_ph_address = set_ph
        self.read_orp_address = set_orp
        self.set_relay = set_relay
        self.plc_all_out = plc_all_out

    def start_ph(self):
        print('-------start PH ORP APF-------')
        #load setting ph
        # response_ph_setting = urlopen(url_substance)
        # ph_json = json.loads(response_ph_setting.read())

        #load time ph
        # response_ph = urlopen(url_ph)
        # data_ph = json.loads(response_ph.read())

        # #load time orp
        # response_orp = urlopen(url_orp)
        # data_orp = json.loads(response_orp.read())

        # #load time apf
        # response_apf = urlopen(url_apf)
        # data_apf = json.loads(response_apf.read())

        #load all api 
        resp = urlopen(url_inj)
        json_decode = json.loads(resp.read())
        
        ph_json = json_decode['get_substance']
        data_ph = json_decode['phs']
        data_orp = json_decode['orps']
        data_apf =  json_decode['apfs']
        chlorine = json_decode['chlorines']
        aco = json_decode['acos']

        
        #read status 8 relay
        relay8 = self.set_relay
      
        if self.current_time >= "00:00" and self.current_time <= '00:59':
            self.process_woking(data_ph[0]['ph_1'], ph_json, relay8, data_orp[0]['orp_1'], data_apf[0]['apf_1'],chlorine[0]['chlorine_1'],aco[0]['aco_1'])
        elif self.current_time >= "01:00" and self.current_time <= '01:59':
            self.process_woking(data_ph[0]['ph_2'], ph_json, relay8, data_orp[0]['orp_2'], data_apf[0]['apf_2'],chlorine[0]['chlorine_2'],aco[0]['aco_2'])
        elif self.current_time >= "02:00" and self.current_time <= '02:59':
            self.process_woking(data_ph[0]['ph_3'], ph_json, relay8, data_orp[0]['orp_3'], data_apf[0]['apf_3'],chlorine[0]['chlorine_3'],aco[0]['aco_3'])
        elif self.current_time >= "03:00" and self.current_time <= '03:59':
            self.process_woking(data_ph[0]['ph_4'], ph_json, relay8, data_orp[0]['orp_4'], data_apf[0]['apf_4'],chlorine[0]['chlorine_4'],aco[0]['aco_4'])
        elif self.current_time >= "04:00" and self.current_time <= '04:59':
            self.process_woking(data_ph[0]['ph_5'], ph_json, relay8, data_orp[0]['orp_5'], data_apf[0]['apf_5'],chlorine[0]['chlorine_5'],aco[0]['aco_5'])
        elif self.current_time >= "05:00" and self.current_time <= '05:59':
            self.process_woking(data_ph[0]['ph_6'], ph_json, relay8, data_orp[0]['orp_6'], data_apf[0]['apf_6'],chlorine[0]['chlorine_6'],aco[0]['aco_6'])
        elif self.current_time >= "06:00" and self.current_time <= '06:59':
            self.process_woking(data_ph[0]['ph_7'], ph_json, relay8, data_orp[0]['orp_7'], data_apf[0]['apf_7'],chlorine[0]['chlorine_7'],aco[0]['aco_7'])
        elif self.current_time >= "07:00" and self.current_time <= '07:59':
            self.process_woking(data_ph[0]['ph_8'], ph_json, relay8, data_orp[0]['orp_8'], data_apf[0]['apf_8'],chlorine[0]['chlorine_8'],aco[0]['aco_8'])
        elif self.current_time >= "08:00" and self.current_time <= '08:59':
           self.process_woking(data_ph[0]['ph_9'], ph_json, relay8, data_orp[0]['orp_9'], data_apf[0]['apf_9'],chlorine[0]['chlorine_9'],aco[0]['aco_9'])
        elif self.current_time >= "09:00" and self.current_time <= '09:59':
            self.process_woking(data_ph[0]['ph_10'], ph_json, relay8, data_orp[0]['orp_10'], data_apf[0]['apf_10'],chlorine[0]['chlorine_10'],aco[0]['aco_10'])
        elif self.current_time >= "10:00" and self.current_time <= '10:59':
            self.process_woking(data_ph[0]['ph_11'], ph_json, relay8, data_orp[0]['orp_11'], data_apf[0]['apf_11'],chlorine[0]['chlorine_11'],aco[0]['aco_11'])
        elif self.current_time >= "11:00" and self.current_time <= '11:59':
            self.process_woking(data_ph[0]['ph_12'], ph_json, relay8, data_orp[0]['orp_12'], data_apf[0]['apf_12'],chlorine[0]['chlorine_12'],aco[0]['aco_12'])
        elif self.current_time >= "12:00" and self.current_time <= '12:59':
            self.process_woking(data_ph[0]['ph_13'], ph_json, relay8, data_orp[0]['orp_13'], data_apf[0]['apf_13'],chlorine[0]['chlorine_13'],aco[0]['aco_13'])
        elif self.current_time >= "13:00" and self.current_time <= '13:59':
            self.process_woking(data_ph[0]['ph_14'], ph_json, relay8, data_orp[0]['orp_14'], data_apf[0]['apf_14'],chlorine[0]['chlorine_14'],aco[0]['aco_14'])
        elif self.current_time >= "14:00" and self.current_time <= '14:59':
            self.process_woking(data_ph[0]['ph_15'], ph_json, relay8, data_orp[0]['orp_15'], data_apf[0]['apf_15'],chlorine[0]['chlorine_15'],aco[0]['aco_15'])
        elif self.current_time >= "15:00" and self.current_time <= '15:59':
            self.process_woking(data_ph[0]['ph_16'], ph_json, relay8, data_orp[0]['orp_16'], data_apf[0]['apf_16'],chlorine[0]['chlorine_16'],aco[0]['aco_16'])
        elif self.current_time >= "16:00" and self.current_time <= '16:59':
            self.process_woking(data_ph[0]['ph_17'], ph_json, relay8, data_orp[0]['orp_17'], data_apf[0]['apf_17'],chlorine[0]['chlorine_17'],aco[0]['aco_17'])
        elif self.current_time >= "17:00" and self.current_time <= '17:59':
            self.process_woking(data_ph[0]['ph_18'], ph_json, relay8, data_orp[0]['orp_18'], data_apf[0]['apf_18'],chlorine[0]['chlorine_18'],aco[0]['aco_18'])
        elif self.current_time >= "18:00" and self.current_time <= '18:59':
           self.process_woking(data_ph[0]['ph_19'], ph_json, relay8, data_orp[0]['orp_19'], data_apf[0]['apf_19'],chlorine[0]['chlorine_19'],aco[0]['aco_19'])
        elif self.current_time >= "19:00" and self.current_time <= '19:59':
          self.process_woking(data_ph[0]['ph_20'], ph_json, relay8, data_orp[0]['orp_20'], data_apf[0]['apf_20'],chlorine[0]['chlorine_20'],aco[0]['aco_20'])
        elif self.current_time >= "20:00" and self.current_time <= '20:59':
            self.process_woking(data_ph[0]['ph_21'], ph_json, relay8, data_orp[0]['orp_21'], data_apf[0]['apf_21'],chlorine[0]['chlorine_21'],aco[0]['aco_21'])
        elif self.current_time >= "21:00" and self.current_time <= '21:59':
            self.process_woking(data_ph[0]['ph_22'], ph_json, relay8, data_orp[0]['orp_22'], data_apf[0]['apf_22'],chlorine[0]['chlorine_22'],aco[0]['aco_22'])
        elif self.current_time >= "22:00" and self.current_time <= '22:59':
            self.process_woking(data_ph[0]['ph_23'], ph_json, relay8, data_orp[0]['orp_23'], data_apf[0]['apf_23'],chlorine[0]['chlorine_23'],aco[0]['aco_23'])
        elif self.current_time >= "23:00" and self.current_time <= '23:59':
            self.process_woking(data_ph[0]['ph_24'], ph_json, relay8, data_orp[0]['orp_24'], data_apf[0]['apf_24'],chlorine[0]['chlorine_24'],aco[0]['aco_24'])

    def process_woking(self, data_ph, ph_json, relay8, data_orp, data_apf,chlorine, aco):
        if str(data_ph) == "1":
            self.process_ph(ph_json, relay8)
        else:
            if relay8[5] == True:
                modbus_ph.stop_ph()
                
        if str(data_orp) == "1":
            self.process_orp(ph_json, relay8)
        else:
            if relay8[6] == True:
                modbus_orp.stop_orp()
                
        if str(data_apf) == "1":
            self.process_apf(ph_json, relay8)
        else:
            if self.plc_all_out[7] == True:
               mod_inj.start_inj1(0)

        if str(chlorine) == "1":
            self.process_chlorine(ph_json, relay8)
        else:
            if self.plc_all_out[8] == True:
                mod_inj.start_inj2(0)

        if str(aco) == '1':
             self.process_aco(ph_json, relay8)
        else:
            if self.plc_all_out[9] == True:
                mod_inj.start_inj3(0)

    def process_ph(self, ph_json, relay8):
        print('xxxxxxxxx PH sssssss')
        print(ph_json[0]['ph_set'])
        print(relay8)
        print(self.read_ph_address)
       
        read_ph = self.read_ph_address
        print('xxxxxxxxx PH sssssss')
        #อ่าน สถานะ relay
        relay8 = relay8

        if float(read_ph) >= float(ph_json[0]['ph_set']):
            print("------------counter ph start---------------"+str(modbus_ph.read_ph_counter()))
            if int(modbus_ph.read_ph_counter()) == 0:
                print("------------counter ph start---------------"+str(relay8[5]))
                if relay8[5] == False:
                    print("------------counter ph start---------+++++++++++++++++++------")
                    modbus_ph.start_ph()
                modbus_ph.write_ph_counter()
            else :
                if relay8[5] == True:
                    modbus_ph.stop_ph()
                modbus_ph.write_ph_counter()
            if int(modbus_ph.read_ph_counter()) >= (int(ph_json[0]['ph_freq']) * 22)  :
            # if int(modbus_ph.read_ph_counter()) >= 10 :
                modbus_ph.set_ph_counter_zero()
                
        elif float(read_ph) <= float(ph_json[0]['ph_lower']):
            if relay8[5] == True:
                modbus_ph.stop_ph()
                modbus_ph.set_ph_counter_zero()

    def process_orp(self, orp_json,relay8):
        print("ORP working")
        print(orp_json[0]['orp_set'])
        print(relay8)
        print(self.read_orp_address)

        read_orp = self.read_orp_address
        #อ่าน สถานะ relay
        relay8 = relay8
        if float(read_orp) <= float(orp_json[0]['orp_set']):
            print("xxxxxxxx counter xxxxxxs "+str(modbus_orp.read_orp_counter()))
            if modbus_orp.read_orp_counter() == 0:
                if relay8[6] == False:
                    modbus_orp.start_orp()
                modbus_orp.write_orp_counter()
            else :
                if relay8[6] == True:
                    modbus_orp.stop_orp()
                modbus_orp.write_orp_counter()
            #แปลงค่า ครึ่งวิให้เป็น วิ * 2
            if int(modbus_orp.read_orp_counter()) >= (int(orp_json[0]['orp_freq']) * 22) :
            # if int(modbus_orp.read_orp_counter())  >= 10 :
                modbus_orp.set_orp_counter_zero()
        elif float(read_orp) >= float(orp_json[0]['orp_lower']):
            if relay8[6] == True:
                modbus_orp.stop_orp()
                modbus_orp.set_orp_counter_zero()

    def process_apf(self, apf_json,relay8):
        print("------------counter apf start---------------"+str(modbus_ph.read_ph_counter()))
        if int(modbus_ph.read_ph_counter()) == 0:
            print("------------counter ph start---------------"+str(relay8[7]))
            if relay8[7] == False:
                print("------------counter apf start---------+++++++++++++++++++------")
                mod_inj.start_inj1(1)
            modbus_ph.write_ph_counter()
        else :
            if relay8[7] == True:
                mod_inj.start_inj1(0)
            modbus_ph.write_ph_counter()
        if int(modbus_ph.read_ph_counter()) >= (int(apf_json[0]['apf_freq']) * 22)  :
        # if int(modbus_ph.read_ph_counter()) >= 10 :
            modbus_ph.set_ph_counter_zero()
    
    def process_chlorine(self, chlorine_json,relay8):
        print("------------counter apf start---------------"+str(modbus_ph.read_ph_counter()))
        if int(modbus_ph.read_ph_counter()) == 0:
            print("------------counter ph start---------------"+str(relay8[7]))
            if self.plc_all_out[8] == False:
                print("------------counter apf start---------+++++++++++++++++++------")
                mod_inj.start_inj2(1)
            modbus_ph.write_ph_counter()
        else :
            if self.plc_all_out[8] == True:
                mod_inj.start_inj2(0)
            modbus_ph.write_ph_counter()
        if int(modbus_ph.read_ph_counter()) >= (int(chlorine_json[0]['chlorine_freq']) * 22)  :
        # if int(modbus_ph.read_ph_counter()) >= 10 :
            modbus_ph.set_ph_counter_zero()
    
    def process_aco(self, aco_json,relay8):
        print("------------counter apf start---------------"+str(modbus_ph.read_ph_counter()))
        if int(modbus_ph.read_ph_counter()) == 0:
            print("------------counter ph start---------------"+str(relay8[7]))
            if self.plc_all_out[9] == False:
                print("------------counter apf start---------+++++++++++++++++++------")
                mod_inj.start_inj3(1)
            modbus_ph.write_ph_counter()
        else :
            if self.plc_all_out[9] == True:
                mod_inj.start_inj3(0)
            modbus_ph.write_ph_counter()
        if int(modbus_ph.read_ph_counter()) >= (int(aco_json[0]['aco_freq']) * 22)  :
        # if int(modbus_ph.read_ph_counter()) >= 10 :
            modbus_ph.set_ph_counter_zero()
            
   