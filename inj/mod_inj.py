import minimalmodbus
import serial
import sys
sys.path.append('/home/pi/hottub_cocoon_apf/setting/')
from path_url import Path_url

path_url = Path_url()
class Mod_inj():
    def start_inj1(self,status):
        instrument = minimalmodbus.Instrument(path_url.modbus_port,path_url.plc_address)
        instrument.serial.port                     # this is the serial port name
        instrument.serial.baudrate = 9600         # Baud        instrument.serial.bytesize = 8
        instrument.serial.parity   = serial.PARITY_NONE
        instrument.serial.stopbits = 1
        instrument.serial.timeout  = 1
        instrument.write_bit(7,status,5)
   

    def start_inj2(self,status):
        instrument = minimalmodbus.Instrument(path_url.modbus_port,path_url.plc_address)
        instrument.serial.port                     # this is the serial port name
        instrument.serial.baudrate = 9600         # Baud        instrument.serial.bytesize = 8
        instrument.serial.parity   = serial.PARITY_NONE
        instrument.serial.stopbits = 1
        instrument.serial.timeout  = 1
        instrument.write_bit(8,status,5)

    def start_inj3(self,status):
        instrument = minimalmodbus.Instrument(path_url.modbus_port,path_url.plc_address)
        instrument.serial.port                     # this is the serial port name
        instrument.serial.baudrate = 9600         # Baud        instrument.serial.bytesize = 8
        instrument.serial.parity   = serial.PARITY_NONE
        instrument.serial.stopbits = 1
        instrument.serial.timeout  = 1
        instrument.write_bit(9,status,5)

   