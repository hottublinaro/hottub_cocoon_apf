import time
import os
import serial
from pymodbus.client import ModbusSerialClient
send = serial.Serial(
                port="/dev/usb_485",
                baudrate = 9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1)
    
data_bytes = bytes([0x01,0x05,0x00,0x05,0xFF,0x00,0x9C,0x3B])
send.write(data_bytes)
